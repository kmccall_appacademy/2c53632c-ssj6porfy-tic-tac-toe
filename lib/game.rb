require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game

  attr_accessor :board, :current_player, :player_one, :player_two

  def initialize(player_one, player_two)
    @player_one = player_one
    @player_two = player_two
    player_one.mark = :X
    player_two.mark = :O
    @board = Board.new
    @current_player = @player_one
  end

  def play
    current_player.display(board)
    until board.over?
      play_turn
    end
    if game_won?
      game_won?.display(board)
      puts "#{game_won?.name} wins!"
    else
      puts "Boo! Tie game"
    end
  end

  def play_turn
    @board.place_mark(@current_player.get_move, @current_player.mark)
    switch_players!
    current_player.display(board)
  end

  def game_won?
    return @player_one if board.winner == player_one.mark
    return @player_two if board.winner == player_two.mark
    false
  end

  def switch_players!
    if @current_player == @player_one
      @current_player = @player_two
    elsif @current_player == @player_two
      @current_player = @player_one
    end
  end
end
