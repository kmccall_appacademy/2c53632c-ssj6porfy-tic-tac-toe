class HumanPlayer

  attr_reader :name
  attr_accessor :mark

  def initialize(name)
    @name = name
  end

  def display(board)
    temp = board.grid.dup.flatten
    (0..(temp.size - 1)).each do |el|
      if temp[el].nil?
        temp[el] = "         "
      else
        temp[el] = "    #{temp[el]}    "  
      end
    end
    puts '  |     0     |     1     |     2     |'
    puts '---------------------------------------'
    puts "0 | #{temp[0]} | #{temp[1]} | #{temp[2]} |"
    puts '---------------------------------------'
    puts "1 | #{temp[3]} | #{temp[4]} | #{temp[5]} |"
    puts '---------------------------------------'
    puts "2 | #{temp[6]} | #{temp[7]} | #{temp[8]} |"
    puts '---------------------------------------'
  end

  def get_move
    puts 'Where would you like to go?'
    move = gets.chomp
    move.split(",").map(&:to_i)
  end

end
