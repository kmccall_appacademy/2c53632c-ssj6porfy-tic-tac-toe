require "byebug"

class ComputerPlayer
  attr_accessor :mark
  attr_reader :board, :name

  def initialize(name)
    @name = name
  end

  def display(board)
    @board = board
  end

  def get_move
    nil_spots = []
    (0..2).each do |row|
      (0..2).each do |col|
        spot = [row, col]
        nil_spots << spot if board[spot] == nil
      end
    end
    nil_spots.each do |spot|
      return spot if winning_move?(spot)
    end
    nil_spots.sample
  end

  def winning_move?(spot)
    board[spot] = mark
    if board.winner == mark
      board[spot] = nil
      true
    else
      board[spot] = nil
      false
    end
  end

end
