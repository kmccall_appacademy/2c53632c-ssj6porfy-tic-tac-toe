class Board
  attr_reader :grid, :marks

  def self.blank_grid
    Array.new(3) { Array.new(3) }
  end

  def initialize(grid = Board.blank_grid)
    @grid = grid
    @marks = [:X, :O]
  end

  def place_mark(spot, mark)
    if empty?(spot)
      @grid[spot[0]][spot[1]] = mark
    else
      raise "Spot is already played"
    end
  end

  def empty?(spot)
    @grid[spot[0]][spot[1]].nil?
  end

  def [](spot)
    row, col = spot
    grid[row][col]
  end

  def []=(spot, value)
    row, col = spot
    grid[row][col] = value
  end

  def winner
    return row_check if row_check
    return col_check if col_check
    return left_diagonal_check if left_diagonal_check
    return right_diagonal_check if right_diagonal_check
    nil
  end

  def left_diagonal_check
    el = @grid[0][0]
    return el if @grid[1][1] == el && @grid[2][2] == el
    nil
  end

  def right_diagonal_check
    el = @grid[2][0]
    return el if @grid[1][1] == el && @grid[0][2] == el
    nil
  end

  def col_check
    @grid.transpose.each do |col|
      return :X if 0.upto(col.size - 1).all? { |idx| col[idx] == :X }
      return :O if 0.upto(col.size - 1).all? { |idx| col[idx] == :O }
    end
    nil
  end

  def row_check
    @grid.each do |row|
      return :X if 0.upto(row.size - 1).all? { |idx| row[idx] == :X }
      return :O if 0.upto(row.size - 1).all? { |idx| row[idx] == :O }
    end
    nil
  end

  def over?
    grid.flatten.none? { |spot| spot.nil? } || winner
  end

end
